import "./firebaseIndex"
import firebase from "firebase";
import 'firebase/app'
import "firebase/firestore";

export const authMethods = {
  // firebase helper methods go here...

  signup: (email, password, displayName, setErrors, setToken, setUser) => {
      console.log(displayName)
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      //make res asynchonous so that we can make grab the token before saving it.
      .then(async res => {
        console.log(res.user);

        const { uid } = res.user;

        const userRef = firebase.firestore().doc(`users/${uid}`);
        const snapshot = await userRef.get();

        if (!snapshot.exists) {
          try {
            await userRef.set({
              displayName,
              email,
              photoURL: "https://static.thenounproject.com/png/43388-200.png"
            });
          } catch (error) {
            console.error("Error creating user document", error);
          }
        }

        try {
          const userDocument = await firebase
            .firestore()
            .doc(`users/${uid}`)
            .get();

          setUser({
            uid,
            ...userDocument.data()
          });
        } catch (error) {
          console.error("Error fetching user", error);
        }

        const token = await Object.entries(res.user)[5][1].b;
        //set token to localStorage
        await localStorage.setItem("token", token);
        setToken(token);
        //grab token from local storage and set to state.
        console.log(res);
      })
      .catch(err => {
        setErrors(prev => [...prev, err.message]);
      });
  },
  signin: (email, password, setErrors, setToken, setUser) => {
    //change from create users to...
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      //everything is almost exactly the same as the function above
      .then(async res => {
        const { uid } = res.user;
        try {
            const userDocument = await firebase
              .firestore()
              .doc(`users/${uid}`)
              .get();
                console.log(userDocument)
            setUser({
              uid,
              ...userDocument.data()
            });
          } catch (error) {
            console.error("Error fetching user", error);
          }
        const token = await Object.entries(res.user)[5][1].b;
        //set token to localStorage
        await localStorage.setItem("token", token);

        setToken(window.localStorage.token);
      })
      .catch(err => {
        setErrors(prev => [...prev, err.message]);
      });
  },
  //no need for email and password
  signout: (setErrors, setToken) => {
    // signOut is a no argument function
    firebase
      .auth()
      .signOut()
      .then(res => {
        //remove the token
        localStorage.removeItem("token");
        //set the token back to original state
        setToken(null);
      })
      .catch(err => {
        //there shouldn't every be an error from firebase but just in case
        setErrors(prev => [...prev, err.message]);
        //whether firebase does the trick or not i want my user to do there thing.
        localStorage.removeItem("token");
        setToken(null);
        console.error(err.message);
      });
    },
    getItemData: async (setItemData) => {
        try {
            const itemDocument = await firebase
              .firestore()
              .doc(`items/`)
              .get();
  
            setItemData({
                ...itemDocument.data()
            });
          } catch (error) {
            console.error("Error fetching item data", error);
          }
    }
};
