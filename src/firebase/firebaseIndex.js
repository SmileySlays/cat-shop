import firebase from "firebase";
import 'firebase/app'
import "firebase/auth";
import "firebase/firestore";

export const firebaseConfig = {
  apiKey: "AIzaSyDuLcg690VMiIq5i1nKZe-yBVfJmEHmOmw",
    authDomain: "e-commerce-23fd3.firebaseapp.com",
    databaseURL: "https://e-commerce-23fd3.firebaseio.com",
    projectId: "e-commerce-23fd3",
    storageBucket: "e-commerce-23fd3.appspot.com",
    messagingSenderId: "237219615453",
    appId: "1:237219615453:web:233f5d24fd9079b9eb8134",
    measurementId: "G-5SBV3YG9Q9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
firebase.auth()
firebase.firestore()

// export default {
//   firebaseConfig, 
// }


// const provider = new firebase.auth.GoogleAuthProvider();
// export const signInWithGoogle = () => {
//   auth.signInWithPopup(provider);
// };

// const getUserDocument = async uid => {
//   if (!uid) return null;
//   try {
//     const userDocument = await firestore.doc(`users/${uid}`).get();

//     return {
//       uid,
//       ...userDocument.data()
//     };
//   } catch (error) {
//     console.error("Error fetching user", error);
//   }
// };