import React, {useState} from 'react';
import {itemMethods} from '../firebase/itemMethods'

export const itemContext = React.createContext()

const ItemProvider = (props) => {
 const [itemData, setItemData] = useState(null)

  const handleAllItemData = () => {
    return itemMethods.getAllItemData(setItemData)
  }

  return (
    <itemContext.Provider
    value={{
      //replaced test with handleSignup
      handleAllItemData,
      itemData,
      setItemData
    }}>
      {props.children}
    </itemContext.Provider>
  );
};

export default ItemProvider;

