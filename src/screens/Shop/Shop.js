import React, { useContext, useEffect, useState } from "react";
import { itemContext } from "../../providers/ItemProvider";
import firebase from "firebase";

const Shop = props => {
    const [itemData, setItemData] = useState(null);
  //   const {  } = useContext(itemContext)


  useEffect(() => {
    async function fetchData() {
      const itemCollection = await firebase.firestore().collection(`items`);
      itemCollection.onSnapshot(snapshot => {
        const allItemData = [];

        snapshot.forEach(doc => {
          allItemData.push({ ...doc.data(), id: doc.id });
        });
        // console.log(allItemData)
        setItemData(allItemData);
      });
    }

    try {
      fetchData();
    } catch (error) {
      console.error("Error fetching item data", error);
    }
  }, []);

  useEffect(() => { console.log(itemData) }, [itemData])

  return (
    <>
      {itemData && itemData.map(categoryList => {
          
          return(
          Object.keys(categoryList).map(category => {
              if (category != "id") {
                  return (
                categoryList[category].map(item => {
                    console.log(item["name"])
                    return(
                        <>
                    <h2>{item["name"]}</h2>
                    <img src={item["photoURL"]} style={{width:"200px", height:"200px"}}/>
                    <h3>{item["price"]}</h3>
                    </>
                    )
                }))}
            
            }
          ))
      })}
    </>
  );
};

export default Shop;
