import React, {useContext} from 'react';
import {firebaseAuth} from '../../providers/AuthProvider'

const Home = (props) => {

  const {handleSignout, user} = useContext(firebaseAuth)


  console.log(user)
  return (
    <div>
      <h1>{user.displayName}</h1>
      <img src={user.photoURL}/>
      <button onClick={handleSignout}>sign out </button>
    </div>
  );
};

export default Home;