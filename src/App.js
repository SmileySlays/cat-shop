
import React, {useContext} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom'
import Signup from './components/Signup/Signup'
import Login from './components/Login/Login'
import Home from './screens/Home/Home'
import Shop from "./screens/Shop/Shop"
import './App.css';
import {firebaseAuth} from './providers/AuthProvider'

function App() {
  const { token } = useContext(firebaseAuth)
  console.log(token)
  return (
    <>
    {/* switch allows switching which components render.  */}
      <Switch>
        {/* route allows you to render by url path */}

        <Route exact path='/' render={rProps => token === null ? <Login /> : <Home />} />
        <Route exact path='/signin' component={Login} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/shop' component={Shop} />
      </Switch>
    </>
  );
}

export default App;
